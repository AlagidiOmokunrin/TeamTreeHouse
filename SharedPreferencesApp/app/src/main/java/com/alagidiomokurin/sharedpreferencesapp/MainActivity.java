package com.alagidiomokurin.sharedpreferencesapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_EDITTEXT = "KEY_EDITTEXT";

    @Override
    protected void onPause() {
        super.onPause();

        mEditor.putString(KEY_EDITTEXT, mEditText.getText().toString());
        mEditor.apply();
    }

    private static final String PREFS_FILE = "com.alagidiomokurin.sharedpreferencesapp.preferences";
    private EditText mEditText;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mEditText = findViewById(R.id.editText);
        sharedPreferences = getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE);
        mEditor = sharedPreferences.edit();

        String editTextString = sharedPreferences.getString(KEY_EDITTEXT,"");
        mEditText.setText(editTextString);
    }
}
